shinyUI(fluidPage(
  
  titlePanel("Macroeconomic series for Belgium"),
  
  sidebarLayout(
    sidebarPanel(
     radioButtons("source", label = "Data source",
                  choices = c("ECB", "ESTAT", "NBB", "OECD"),
                  selected = "NBB"),
     uiOutput("type"),
     uiOutput("geo"),
     uiOutput("id1"),
     uiOutput("id2"),
     checkboxInput("showgrid", label = "Show Grid", value = TRUE)
    ),
    mainPanel(
      dygraphOutput("dygraph"),
     
      dataTableOutput("legendTable")
    )
  )
))
